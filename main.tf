terraform {
    required_providers {
        aws = {
            source  = "hashicorp/aws"
            version = "~> 3.27"
        }
    }
}

# Configuration AWS Provider
provider "aws" {
    profile = "default"
    region  = "eu-west-3"
}

# creation de l'instance
resource "aws_instance" "test_jl" {
    ami           = "ami-0ec28fc9814fce254"
    instance_type = "t2.micro"

    tags = {
        Name = "Hello_JL"
    }
}

//  ID JL = 865671838769
//
//
